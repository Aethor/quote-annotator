# Quote Annotator

This is an experimental quote annotation program, supporting the [Bert Quote Attribution](https://gitlab.com/Aethor/bert-quote-attribution) project.

This software works on file having the same form as files of the [QuoteLi3 dataset](https://nlp.stanford.edu/~muzny/quoteli.html). 

For each quote, it can add the following annotation :

* An unique speaker,
* Several addressees

It can also add or modify characters.

![The annotator interface](./screenshot.png)
