const { app, Menu, dialog } = require('electron')

exports.buildMainMenu = (mainWindow, openFile, saveFile) => {
    return Menu.buildFromTemplate([
	{
	    label: 'File...',
	    submenu: [
		{
		    label: 'Open...',
		    accelerator: 'Ctrl+O',
		    click () {
			dialog.showOpenDialog({
			    properties: ['openFile']
			}).then((dialogResult) => {
			    if (dialogResult.canceled) { return }
			    filepath = dialogResult.filePaths[0]
			    openFile(filepath)
			}
			).catch(() => {
			    console.log('[error] failed to open file')
			})
		    }
		},
		{
		    label: 'Save as...',
		    accelerator: 'Ctrl+S',
		    click () {
			dialog.showSaveDialog().then((result) => {
			    if (result.filePath === undefined) {
				return console.log('canceled operation')
			    }
			    saveFile(result.filePath)
			}).catch((err) => {
			    console.log('[error] could not save file')
			    console.log(err)
			})
		    }
		},
		{
		    label: 'Debug',
		    accelerator: 'Ctrl+D',
		    click () {
			mainWindow.webContents.openDevTools()
		    }
		},
		{
		    label: 'Quit',
		    accelerator: 'Ctrl+Q',
		    click () {
			app.exit()
		    }
		}
	    ]
	}
    ])
}
