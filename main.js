const { app, BrowserWindow, Menu, ipcMain } = require('electron')
const libxml = require('libxmljs')
const fs = require('fs')

/**
 * @param {string} content
 * @returns {{text: string, characters: Array.<{name: string, aliases: Array.<string>}>}}
 */
function loadDocument(content) {
    let document = libxml.parseXmlString(content);

    let characters = document.find('//character').map((character) => {
	return {
	    name: character.attr('name') !== null ? character.attr('name').value() : undefined,
	    aliases: character.attr('aliases') !== null ? character.attr('aliases').value().split(';') : undefined
	}
    })

    let text = document.find('//text')[0].toString({whitespace: true}).replace(/\n\n/g, '<br />')

    return {
	text: text,
	characters: characters,
    }
}

/**
 * @param {{text: string, characters: Array.<{name: string, aliases: Array.<string>}>}} content
 * @returns {string} 
 */
function stringifyDocument(content) {
    var xmlDoc = new libxml.Document()
    xmlDoc.node('doc').node('characters')
    let characterNode = xmlDoc.get('//characters')
    for (let character of content.characters) {
    	characterNode.node('character').attr({
    	    name: character.name,
    	    aliases: character.aliases.join(';')
    	})
    }
    //xmlDoc.get('//doc').node('text', libxml.parseHtmlFragment(content.text))
    let textNode = libxml.parseXmlString(content.text).get('//text')
    xmlDoc.get('//doc').addChild(textNode)
    return xmlDoc.toString()
}

app.whenReady().then(() => {
    let mainWindow = new BrowserWindow({
	width: 1024,
	height: 768,
	webPreferences: {
	    nodeIntegration: true
	}
    })

    /**
     * @param {string} filepath
     */
    function openFile(filepath) {
	fs.readFile(filepath, {encoding: 'utf8'}, (err, content) => {
	    if (err) { console.log(err); return }
	    let document = loadDocument(content)
	    mainWindow.loadFile('./views/annotation/annotation.html')
	    mainWindow.webContents.on('did-finish-load', () => {
		mainWindow.webContents.send('load-document', document)
	    })
	})
    }

    /**
     * @param {string} filepath
     */
    function saveFile(filepath) {
	mainWindow.webContents.send('save-document')
	ipcMain.once('save-document-reply', (event, reply) => {
	    let content = stringifyDocument(reply)
	    fs.writeFile(filepath, content, (err) => {
		if (err) { return console.log(err); }
		console.log(`[info] correctly saved file ${filepath}`)
	    })
	})
    }

    const mainMenu = require('./menu').buildMainMenu(mainWindow, openFile, saveFile)
    Menu.setApplicationMenu(mainMenu)

    mainWindow.loadFile('index.html')   
})
