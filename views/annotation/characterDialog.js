const {BrowserWindow, ipcMain} = require('electron').remote

/**
 * Show a character modification dialog
 * @paran {{name: {string|undefined}, aliases: {Array.<string>|undefined}}|undefined} initalCharacter
 * @param {Array.<string>|undefined} initialAliases
 * @return {Promise}
 */
function showCharacterDialog(initialCharacter = undefined) {
    let characterWin = new BrowserWindow({
	width: 300,
	height: 200,
	frame: false,
	webPreferences: {
	    nodeIntegration: true
	}
    })
    characterWin.loadFile('views/annotation/characterDialog.html')
    return new Promise((resolve, reject) => {
	characterWin.webContents.on('did-finish-load', () => {
	    if (initialCharacter === undefined){
		characterWin.webContents.send('character-dialog-load-initial-values', {
		    name: undefined,
		    aliases: undefined
		})
	    } else{ 
		characterWin.webContents.send('character-dialog-load-initial-values', {
		    name: initialCharacter.name,
		    aliases: initialCharacter.aliases
		})
	    }
	    ipcMain.once('character-dialog-finished', (event, returnedObject) => {
		characterWin.close()
		if (returnedObject.status === 'CANCELED') {
		    reject(returnedObject.status)
		} else if (returnedObject.status === 'OK') {
		    resolve(returnedObject.character)
		} else {
		    console.log(`[warning] unknown status ${returnedObject.status}`)
		}
	    })
	})
    })
}

exports.showCharacterDialog = showCharacterDialog
