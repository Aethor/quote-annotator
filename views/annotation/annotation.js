const {ipcRenderer} = require('electron');

ipcRenderer.on('load-document', (event, doc) => {
    //allow iteration over HTMLCollection
    HTMLCollection.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];

    //global state
    var selectedQuote = null

    //build UI
    displayCharacters(doc.characters)
    displayText(doc.text)
});

ipcRenderer.on('save-document', (event) => {
    console.log('received save request')
    event.sender.send('save-document-reply', {
	characters: getCharacters(),
	text: document.getElementsByTagName('text')[0].outerHTML.replace(/<br>/g, '\n\n')
    })
})

/**
 * @param {} selectNode
 * @param {string} option
 */
function setSelectNodeOption(selectNode, option) {
    for (let j = 0; j < selectNode.options.length; j++) {
	if (selectNode.options[j].textContent === option) {
	    selectNode.selectedIndex = j
	    break
	}
    }
}

/**
 * @returns {{name: {string|undefined}, aliases: {Array.<string>|undefined}}}
 */
function getCharacters() {
    let charactersNode = document.getElementsByClassName('character')
    characters = []
    for (let characterNode of charactersNode) {
    	characters.push(characterFromCharacterNode(characterNode))
    }
    return characters
}

function characterSelectorNode() {
    let characterSelectorNode = document.createElement('select')
    for (let character of getCharacters()) {
	let optionNode = document.createElement('option')
	optionNode.textContent = character.name
	characterSelectorNode.appendChild(optionNode)
    }
    return characterSelectorNode
}

function refreshSelectedQuoteAddressees() {
    if (!selectedQuote) { return }
    addressees = []
    for (let addresseeNode of document.getElementsByClassName('addressee')) {
	addressees.push(addresseeNode.selectedOptions[0].textContent)
    }
    selectedQuote.setAttribute('addressees', addressees.join(';'))
}

/**
 * @returns {HTMLSelectElement}
 */
function addAddresseeNode() {
    let quoteViewAddressees = document.getElementById('quote-view-addressees')

    // Create dropdown list
    let newAddresseeNode = characterSelectorNode()
    quoteViewAddressees.appendChild(newAddresseeNode)
    newAddresseeNode.classList.add('addressee')
    if (newAddresseeNode.children.length > 0) {
	setSelectNodeOption(newAddresseeNode, newAddresseeNode.firstChild.textContent)
	refreshSelectedQuoteAddressees()
    }
    newAddresseeNode.addEventListener('change', () => {
	refreshSelectedQuoteAddressees()
    })

    // Create remove addressee button
    let removeAddresseeButton = document.createElement('button')
    quoteViewAddressees.appendChild(removeAddresseeButton)
    removeAddresseeButton.textContent = '-'
    removeAddresseeButton.addEventListener('click', () => {
	quoteViewAddressees.removeChild(newAddresseeNode)
	quoteViewAddressees.removeChild(removeAddresseeButton)
	refreshSelectedQuoteAddressees()
    })

    return newAddresseeNode
}

/**
 * @param {HTMLCollection} characterNode
 * @returns {{name: {string|undefined}, aliases: {Array.<string>|undefined}}} character
 */
function characterFromCharacterNode(characterNode) {
    return {
	name: characterNode.textContent ? characterNode.textContent : undefined,
	aliases: characterNode.getAttribute("aliases") ? characterNode.getAttribute("aliases").split(';') : undefined
    }
}

/**
 * @param {{name: {string|undefined}, aliases: {Array.<string>|undefined}}} character
 * @returns {HTMLCollection}
 */
function characterNodeFromCharacter(character) {
    let characterNode = document.createElement('p')
    characterNode.classList.add('character')
    if (character.name) {
	characterNode.textContent = character.name
    }
    if (character.aliases) {
	characterNode.setAttribute("aliases", character.aliases.join(';'))
    }
    return characterNode
}

function editCharacterNode(characterNode) {
    require('./characterDialog').showCharacterDialog(
	characterFromCharacterNode(characterNode)
    ).then((returnedCharacter) => {
	let oldName = characterNode.textContent
	let stringAliases = returnedCharacter.aliases.join(';')
	characterNode.textContent = returnedCharacter.name 
	characterNode.attributes.aliases = stringAliases
	// update each quote to reflect new character
	for (let quote of document.getElementsByTagName('quote')) {
	    if (quote.getAttribute('speaker') === oldName) {
		quote.setAttribute('speaker', returnedCharacter.name)
	    }

	    if (!quote.getAttribute('addressees')) {continue}
	    addressees = quote.getAttribute('addressees').split(';')
	    for (let addressee of addressees) {
		if (addressee !== oldName){continue}
		addressee = returnedCharacter.name
	    }
	    quote.setAttribute('addressees', addressees.join(';'))
	}
	// reload quote display UI
	displayQuote(selectedQuote)
    }).catch((status) => {
	console.log(`[info] character properties were not set with status ${status}`)
    })
}

/**
 * @param {{name: {string}, aliases: {Array.<string>}}}
 * @returns HTMLElement
 */
function addCharacterNode(character) {
    let characterViewCharacters = document.getElementById('character-view-characters')

    // Create character node
    let characterNode = characterNodeFromCharacter(character)
    characterNode.addEventListener('click', () => {
	editCharacterNode(characterNode)
    })
    characterViewCharacters.appendChild(characterNode)

    // Create button to remove character later
    let characterRemoveButton = document.createElement('button')
    characterRemoveButton.classList.add('characterRemoveButton')
    characterRemoveButton.textContent = '-'
    var selectedQuote = selectedQuote
    characterRemoveButton.addEventListener('click', () => {
	// update each quote to reflect character deletion
	for (let quote of document.getElementsByTagName('quote')) {
	    if (quote.getAttribute('speaker') === characterNode.textContent) {
		quote.setAttribute('speaker', '')
	    }

	    if (!quote.getAttribute('addressees')) {continue}
	    let addressees = quote.getAttribute('addressees').split(';')
	    addressees = addressees.filter((el) => {el !== characterNode.textContent})
	    quote.setAttribute('addressees', addressees.join(';'))
	}
	// reload quote display UI
	displayQuote(selectedQuote)
	// delete character node + remove button node
	characterViewCharacters.removeChild(characterNode)
	characterViewCharacters.removeChild(characterRemoveButton)
    })
    characterViewCharacters.appendChild(characterRemoveButton)
    return characterNode
}

function displayCharacters(characters) {
    let characterView = document.getElementById('character-view')
    let characterViewAddCharacterButton = document.getElementById('character-view-add-character-button')
    let characterViewCharacters = document.getElementById('character-view-characters')
    //clear characterView
    characterViewAddCharacterButton.textContent = ''
    characterViewCharacters.textContent = ''

    //add addCharacterButton to top of characterView
    let addCharacterButton = document.createElement('button')
    addCharacterButton.textContent = '+'
    addCharacterButton.addEventListener("click", () => {
	require('./characterDialog').showCharacterDialog().then((character) => {
	    addCharacterNode(character)
	}).catch(() => {
	    console.log('cancelled operation')
	})
    })
    characterViewAddCharacterButton.appendChild(addCharacterButton)

    //add all characters to characterView
    for (let character of characters) {
	addCharacterNode(character)
    }
}

function displayText(text) {
    document.getElementById('document-view').innerHTML = text
    for (let quote of document.getElementsByTagName('quote')) {
	quote.addEventListener("click", () => {
	    displayQuote(quote)
	})
    }
}

function displayQuote(quoteElement) {
    if (!quoteElement) { return }
    // Selected quote state
    selectedQuote = quoteElement

    // Display quote text
    document.getElementById('quote-view-text').textContent = quoteElement.textContent

    let quoteViewSpeaker = document.getElementById('quote-view-speaker')
    //Clear speaker options
    quoteViewSpeaker.textContent = ''
    // Create all speaker options
    for (let character of getCharacters()) {
	let optionNode = document.createElement('option')
	optionNode.textContent = character.name
	quoteViewSpeaker.appendChild(optionNode)
    }
    // Set dropbox selected option to be quote speaker
    let speaker = quoteElement.getAttribute('speaker')
    setSelectNodeOption(quoteViewSpeaker, speaker)
    
    // Set dropbox selection to change quote
    quoteViewSpeaker.addEventListener('change', () => {
	selectedQuote.setAttribute('speaker', quoteViewSpeaker.selectedOptions[0].textContent)
    })

    let quoteViewAddressees = document.getElementById('quote-view-addressees')
    quoteViewAddressees.textContent = ''
    if (!quoteElement.getAttribute('addressees')) {
	quoteElement.setAttribute('addressees', '')
    }
    let addressees = quoteElement.getAttribute('addressees').split(';').filter((x) => x)
    for (let j = 0; j < addressees.length; j++) {
	let addresseeNode = addAddresseeNode()
	setSelectNodeOption(addresseeNode, addressees[j])
	refreshSelectedQuoteAddressees()
    }
    let addAddresseeButton = document.createElement('button')
    addAddresseeButton.textContent = '+'
    addAddresseeButton.addEventListener('click', () => {
	addAddresseeNode()
    })
    quoteViewAddressees.prepend(addAddresseeButton)
}


